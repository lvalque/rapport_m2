\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Need for 3D Snap Rounding}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Formal Statement}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}2D Snap Rounding}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}3D Snap Rounding}{4}{subsection.1.4}
\contentsline {section}{\numberline {2}3D Snap Rounding Algorithm}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Introduction}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Step 1: Collapse the faces that are close to one another}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Step 2: Partition the space into xy-parallel slabs}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Step 3a: 2D snap in thin slab to prepare triangulation}{6}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Step 3b: Triangulation of the faces in thick slabs }{7}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Correctness and complexity}{8}{subsection.2.6}
\contentsline {section}{\numberline {3}Algorithm implementation}{9}{section.3}
\contentsline {subsection}{\numberline {3.1}2D Snap Rounding Implementation}{9}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Step 1 Implementation}{10}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Difficulties}{10}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Step 1 in a Thick Slab}{11}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Segment Tree}{13}{subsubsection.3.2.3}
\contentsline {subsection}{\numberline {3.3}Step 2 implementation}{13}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Arrangement on the floor}{13}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Lift the arrangement}{13}{subsubsection.3.3.2}
\contentsline {subsection}{\numberline {3.4}Efficient cut of Faces}{14}{subsection.3.4}
\contentsline {section}{\numberline {4}Result of the implementation}{15}{section.4}
\contentsline {section}{\numberline {5}Improvement of the algorithm and the implementation}{17}{section.5}
\contentsline {subsection}{\numberline {5.1}Lazy algorithm}{17}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Test of proper intersection}{18}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Lifting only in \textit {thick slabs}}{19}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Avoid exact calculations}{19}{subsection.5.4}
\contentsline {section}{\numberline {6}Summary of the work performed}{19}{section.6}
\contentsline {section}{\numberline {7}Conclusions and Perspectives}{20}{section.7}
\contentsline {part}{Appendix}{22}{part*.25}
\contentsline {section}{\numberline {A}Polygon difference}{22}{appendix.A}
\contentsline {section}{\numberline {B}Halfedge Data structure}{22}{appendix.B}
\contentsline {section}{\numberline {C}Manage degeneration created by Step 1}{22}{appendix.C}
