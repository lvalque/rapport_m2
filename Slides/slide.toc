\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Need for 3D Snap Rounding}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{State of the art}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Formal Statement}{7}{0}{1}
\beamer@subsectionintoc {1}{4}{2D Snap Rounding}{12}{0}{1}
\beamer@subsectionintoc {1}{5}{3D Snap Rounding Difficulties}{20}{0}{1}
\beamer@sectionintoc {2}{3D Snap Rounding Algorithm}{26}{0}{2}
\beamer@sectionintoc {3}{Implementation Issue}{47}{0}{3}
\beamer@subsectionintoc {3}{1}{CGAL Library}{48}{0}{3}
\beamer@subsectionintoc {3}{2}{2D Snap Rounding}{52}{0}{3}
\beamer@subsectionintoc {3}{3}{Halfedge data structure}{54}{0}{3}
\beamer@subsectionintoc {3}{4}{Arrangement on the floor}{55}{0}{3}
\beamer@sectionintoc {4}{Algorithm Issue}{62}{0}{4}
\beamer@subsectionintoc {4}{1}{Step 1 difficulties}{63}{0}{4}
\beamer@subsectionintoc {4}{2}{Step 1 in thick slab}{65}{0}{4}
\beamer@sectionintoc {5}{Result}{70}{0}{5}
\beamer@sectionintoc {6}{Possible Improvements}{78}{0}{6}
\beamer@subsectionintoc {6}{1}{A Lazy algorithm}{79}{0}{6}
\beamer@subsectionintoc {6}{2}{Other improvements}{81}{0}{6}
\beamer@sectionintoc {7}{Conclusions}{84}{0}{7}
